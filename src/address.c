/*****************************************************************************
 *   Ledger App Boilerplate.
 *   (c) 2020 Ledger SAS.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *****************************************************************************/

#include <stdint.h>   // uint*_t
#include <stddef.h>   // size_t
#include <stdbool.h>  // bool
#include <string.h>   // memmove

#include "os.h"
#include "cx.h"

#include "address.h"

#include "transaction/types.h"
#include "common/base58.h"

void hash_compressed_public_key(const uint8_t *in, unsigned short inlen, unsigned char *out) {
    cx_ripemd160_t riprip;
    unsigned char buffer[32];
    PRINTF("To hash\n%.*H\n", inlen, in);
    cx_hash_sha256(in, inlen, buffer, 32);
    cx_ripemd160_init(&riprip);
    cx_hash(&riprip.header, CX_LAST, buffer, 32, out, 20);
    PRINTF("Hash160\n%.*H\n", 20, out);
}

void compute_checksum(unsigned char *in, unsigned short inlen, unsigned char *output) {
    unsigned char checksumBuffer[32];
    cx_hash_sha256(in, inlen, checksumBuffer, 32);
    cx_hash_sha256(checksumBuffer, 32, checksumBuffer, 32);
    PRINTF("Checksum\n%.*H\n", 4, checksumBuffer);
    memmove(output, checksumBuffer, 4);
}
static bool compress_public_key(const uint8_t raw_public_key[static PUBKEY_UNCOMPRESSED_LEN],
                                uint8_t *out,
                                size_t out_len) {
    if (out_len < PUBKEY_COMPRESSED_LEN) {
        return false;
    }
    // Because the elliptic curve is symmetrical along its x axis, we only need the x coordinate and
    // the indicator of y coordinate position
    out[0] = (raw_public_key[63] & 0x1) ? 0x03 : 0x02;
    memmove(out + 1, raw_public_key, PUBKEY_COMPRESSED_LEN - 1);
    return true;
}

// address = bitcoinEncode( ripemd160 ( sha256 ( publicKey ) ) )
bool address_from_pubkey(const uint8_t public_key[static 64], uint8_t *out, size_t out_len) {
    if (out_len < ADDRESS_LEN + 1) {
        return false;
    }

    uint8_t compressed_public_key[PUBKEY_COMPRESSED_LEN] = {0};
    uint8_t address[1 + 20 + 4] = {0};

    compress_public_key(public_key, compressed_public_key, PUBKEY_COMPRESSED_LEN);
    hash_compressed_public_key(compressed_public_key, PUBKEY_COMPRESSED_LEN, address + 1);
    address[0] = 0x00;
    compute_checksum(address, 20 + 1, address + 20 + 1);

    if (base58_encode(address, 1 + 20 + 4, (char *) out, out_len) < 0) {
        THROW(EXCEPTION);
    }
    return true;
}
